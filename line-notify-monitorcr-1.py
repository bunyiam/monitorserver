#!/usr/bin/python
# -*- coding: utf-8 -*-

import psutil
import requests

url = 'https://notify-api.line.me/api/notify'
token = ''
headers = {'content-type':'application/x-www-form-urlencoded','Authorization':'Bearer '+token}

cpu_percent = str (psutil.cpu_percent())
virtual_memory = str (psutil.virtual_memory())
msg = cpu_percent + virtual_memory
r = requests.post(url, headers=headers , data = {'message':msg})
print r.text
