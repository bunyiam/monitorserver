#!/usr/bin/python
# -*- coding: utf-8 -*-

import psutil
import platform
import datetime
import requests

url = 'https://notify-api.line.me/api/notify'
token = ''
headers = {'content-type':'application/x-www-form-urlencoded','Authorization':'Bearer '+token}

os, name, version, _, _, _ = platform.uname()
version = version.split('-')[0]
cores = psutil.cpu_count()
cpu_percent = psutil.cpu_percent()
memory_percent = psutil.virtual_memory()[2]
disk_percent = psutil.disk_usage('/')[3]
boot_time = datetime.datetime.fromtimestamp(psutil.boot_time())
running_since = boot_time.strftime("%A %d. %B %Y")
response = "%s version %s.\n" % (os, version)
response += "Server  %s with %s CPU cores.\n" % (name, cores)
response += "Disk usage is %s percent.\n" % disk_percent
response += "CPU usage is %s percent.\n" % cpu_percent
response += "Memory usage is %s percent.\n" % memory_percent
#response += "Online since %s." % running_since

check = float(memory_percent)

if check > float(3.0):
    r = requests.post(url, headers=headers , data = {'message':response})
    print r.text
else:
     print('ok')

#crontab -e
#1 10 * * * /usr/bin/python /root/line-notify-monitorcr.py > /dev/null
#1 14 * * * /usr/bin/python /root/line-notify-monitorcr.py > /dev/null